using s1voded.home;
using s1voded.home.config;
using System.Reflection;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Extensions.Http;
using InfluxDB.Client;
using s1voded.home.service;
using s1voded.home.service.Aqara;
using s1voded.home.service.InfluxDb;

public class Program
{
    public static async Task Main(string[] args)
    {
        var config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true)
            .AddUserSecrets(Assembly.GetExecutingAssembly(), true)
            .AddEnvironmentVariables()
            .Build();

        IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
                .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
        }

        IHost host = Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(builder =>
            {
                builder.AddConfiguration(config);
            })
            .ConfigureServices(services =>
            {
                services
                //config
                .Configure<InfluxConfig>(config.GetSection(nameof(InfluxConfig)))
                .Configure<AqaraConfig>(config.GetSection(nameof(AqaraConfig)))
                .Configure<AqaraSecret>(config.GetSection(nameof(AqaraSecret)))

                //main background worker
                .AddHostedService<MainWorker>()

                //influxDb
                .AddSingleton<IInfluxDbService, InfluxDbService>()
                .AddSingleton<IInfluxDBClient>(sp =>
                {
                    var influxConfig = sp.GetService<IOptions<InfluxConfig>>().Value;
                    return InfluxDBClientFactory.Create(influxConfig.URL, influxConfig.AdminToken);
                })

                //aqara api service
                .AddHttpClient<IAqaraApiService, AqaraApiService>((sp, httpClient) =>
                {
                    var aqaraConfig = sp.GetService<IOptions<AqaraConfig>>().Value;
                    var aqaraSecret = sp.GetService<IOptions<AqaraSecret>>().Value;

                    httpClient.BaseAddress = new Uri(aqaraConfig.URI);
                    httpClient.DefaultRequestHeaders.Add(nameof(AqaraConfig.AppId), aqaraConfig.AppId);
                    httpClient.DefaultRequestHeaders.Add(nameof(AqaraConfig.KeyId), aqaraConfig.KeyId);
                    httpClient.DefaultRequestHeaders.Add(nameof(AqaraSecret.AccessToken), aqaraSecret.AccessToken);
                    httpClient.DefaultRequestHeaders.Add("Lang", "ru");
                }).AddPolicyHandler(GetRetryPolicy());
            })
            .Build();

        await host.RunAsync();
    }
}