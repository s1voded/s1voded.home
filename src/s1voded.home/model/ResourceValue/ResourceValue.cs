﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model.ResourceValue
{
    public class ResourceValue: IAqaraResponseArrayItem
    {
        [JsonProperty("subjectId")]
        public string SubjectId { get; set; }

        [JsonProperty("resourceId")]
        public string ResourceId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("timeStamp")]
        public long TimeStamp { get; set; }
    }
}
