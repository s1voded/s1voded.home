﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model.ResourceValue
{
    public class ResourceValueRequestData: IAqaraRequestData
    {
        [JsonProperty("resources")]
        public IList<ResourceValueRequestItem> Resources { get; set; }
    }
}
