﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model.ResourceValue
{
    public class ResourceValueRequestItem
    {
        [JsonProperty("subjectId")]
        public string SubjectId { get; set; }

        [JsonProperty("resourceIds")]
        public IList<string> ResourceIds { get; set; }
    }
}
