﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model
{
    public class AqaraResponseArrayModel<T> : AqaraBaseResponseModel where T : IAqaraResponseArrayItem
    {
        [JsonProperty("result")]
        public IList<T> Result { get; set; }
    }

    //Marker interface
    public interface IAqaraResponseArrayItem
    {

    }
}
