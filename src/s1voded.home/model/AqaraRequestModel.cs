﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model
{
    public class AqaraRequestModel<T> where T : IAqaraRequestData
    {
        [JsonProperty("intent")]
        public string Intent { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }
    }

    //Marker interface
    public interface IAqaraRequestData
    {

    }
}
