﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model.DeviceInfo
{
    public class DeviceInfoResponseResult : IAqaraResponseResult
    {
        [JsonProperty("data")]
        public IList<DeviceInfo> Data { get; set; }

        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
    }
}
