﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model.DeviceInfo
{
    public class DeviceInfo
    {
        /// <summary>
        /// Gateway id
        /// </summary>
        [JsonProperty("parentDid")]
        public string ParentDid { get; set; }

        [JsonProperty("createTime")]
        public long CreateTime { get; set; }

        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }

        [JsonProperty("model")]
        public string Model { get; set; }

        [JsonProperty("updateTime")]
        public long UpdateTime { get; set; }

        [JsonProperty("modelType")]
        public int ModelType { get; set; }

        /// <summary>
        /// Status: 0-offline 1-online
        /// </summary>
        [JsonProperty("state")]
        public int State { get; set; }

        /// <summary>
        /// Device id
        /// </summary>
        [JsonProperty("did")]
        public string Did { get; set; }

        [JsonProperty("deviceName")]
        public string DeviceName { get; set; }
    }
}
