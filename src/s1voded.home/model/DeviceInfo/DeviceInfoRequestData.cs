﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model.DeviceInfo
{
    public class DeviceInfoRequestData : IAqaraRequestData
    {
        [JsonProperty("dids", NullValueHandling = NullValueHandling.Ignore)]
        public IList<string> Dids { get; set; }

        [JsonProperty("positionId", NullValueHandling = NullValueHandling.Ignore)]
        public string PositionId { get; set; }

        [JsonProperty("pageNum")]
        public int PageNum { get; set; }

        [JsonProperty("pageSize")]
        public int PageSize { get; set; }
    }
}
