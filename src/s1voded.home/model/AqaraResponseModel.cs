﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model
{
    public class AqaraResponseModel<T> : AqaraBaseResponseModel where T : IAqaraResponseResult
    {
        [JsonProperty("result")]
        public T Result { get; set; }
    }

    //Marker interface
    public interface IAqaraResponseResult
    {

    }
}
