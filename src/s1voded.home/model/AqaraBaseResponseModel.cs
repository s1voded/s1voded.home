﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.model
{
    public class AqaraBaseResponseModel
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("msgDetails")]
        public string? MsgDetails { get; set; }

        [JsonProperty("requestId")]
        public string RequestId { get; set; }
    }
}
