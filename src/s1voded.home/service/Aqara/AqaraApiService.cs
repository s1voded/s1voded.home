﻿using Newtonsoft.Json;
using s1voded.home.config;
using s1voded.home.model.DeviceInfo;
using s1voded.home.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using s1voded.home.util;
using System.Net.Http.Headers;
using s1voded.home.model.ResourceValue;
using Microsoft.VisualBasic;

namespace s1voded.home.service.Aqara
{
    public class AqaraApiService : IAqaraApiService
    {
        private readonly HttpClient _httpClient;
        private readonly IOptions<AqaraConfig> _aqaraConfig;
        private readonly IOptions<AqaraSecret> _aqaraSecret;

        public AqaraApiService(HttpClient httpClient, IOptions<AqaraConfig> aqaraConfig, IOptions<AqaraSecret> aqaraSecret)
        {
            _httpClient = httpClient;
            _aqaraConfig = aqaraConfig;
            _aqaraSecret = aqaraSecret;
        }

        //Generate and add custom request headers: https://opendoc.aqara.com/en/docs/developmanual/apiIntroduction/signGenerationRules.html
        private void AddCustomRequestHeaders(HttpRequestHeaders httpRequestHeaders)
        {
            var now = (DateTimeOffset)DateTime.UtcNow;
            var unix_ms = now.ToUnixTimeMilliseconds().ToString();
            var sign = MD5SignHelper.CreateSign(_aqaraSecret.Value.AccessToken, _aqaraConfig.Value.AppId, _aqaraConfig.Value.KeyId, unix_ms, unix_ms, _aqaraSecret.Value.AppKey);

            httpRequestHeaders.Add("Nonce", unix_ms);
            httpRequestHeaders.Add("Time", unix_ms);
            httpRequestHeaders.Add("Sign", sign);
        }

        // Query the basic information of the device, including device ID, position, offline status, firmware version, time zone, etc.
        public async Task<AqaraResponseModel<DeviceInfoResponseResult>> GetAllDevicesInfoAsync()
        {
            var aqaraRequestAllDeviceInfo = new AqaraRequestModel<DeviceInfoRequestData>()
            {
                Intent = Constant.RequestIntent_QueryDeviceInfo,
                Data = new DeviceInfoRequestData() { PageNum = 1, PageSize = 50 } //default values
            };

            var stringContent = JsonConvert.SerializeObject(aqaraRequestAllDeviceInfo);//сериализуем объект в строку

            var jsonResponse = await PostAsyncHelper(stringContent);

            return JsonConvert.DeserializeObject<AqaraResponseModel<DeviceInfoResponseResult>>(jsonResponse);            
        }

        // Query the current status of the specified resource of the device.
        public async Task<AqaraResponseArrayModel<ResourceValue>> GetDeviceResourceValueAsync(List<DeviceInfo> devicesInfo)
        {
            var resourcesValueRequestList = new List<ResourceValueRequestItem>();

            foreach(var deviceInfo in devicesInfo)
            {
                var resourcesValueReqestItem = new ResourceValueRequestItem()
                {
                    ResourceIds = new List<string>(),//If it is empty, query all open resources of the device.
                    SubjectId = deviceInfo.Did
                };

                resourcesValueRequestList.Add(resourcesValueReqestItem);
            }

            var requestDeviceResourceValue = new AqaraRequestModel<ResourceValueRequestData>()
            {
                Intent = Constant.RequestIntent_QueryResourceValue,
                Data = new ResourceValueRequestData() { Resources = resourcesValueRequestList }
            };

            var stringContent = JsonConvert.SerializeObject(requestDeviceResourceValue);//сериализуем объект в строку

            var jsonResponse = await PostAsyncHelper(stringContent);

            return JsonConvert.DeserializeObject<AqaraResponseArrayModel<ResourceValue>>(jsonResponse);
        }

        private async Task<string> PostAsyncHelper(string stringContent)
        {
            using (var httpContent = new StringContent(stringContent, Encoding.UTF8, "application/json"))
            {
                var httpRequestMessage = new HttpRequestMessage()
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri(_aqaraConfig.Value.URI),
                    Content = httpContent
                };

                AddCustomRequestHeaders(httpRequestMessage.Headers);

                var httpResponse = await _httpClient.SendAsync(httpRequestMessage);

                if (httpResponse != null)
                {
                    var jsonResponse = await httpResponse.Content.ReadAsStringAsync();

                    if (httpResponse.IsSuccessStatusCode)
                    {
                        return jsonResponse;
                    }
                    else
                    {
                        httpResponse.EnsureSuccessStatusCode();//кидает exception если !httpResponse.IsSuccessStatusCode
                    }
                }
            }

            return "";
        }
    }
}
