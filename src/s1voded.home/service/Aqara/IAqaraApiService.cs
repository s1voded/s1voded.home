﻿using s1voded.home.model.DeviceInfo;
using s1voded.home.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using s1voded.home.model.ResourceValue;

namespace s1voded.home.service.Aqara
{
    public interface IAqaraApiService
    {
        Task<AqaraResponseModel<DeviceInfoResponseResult>> GetAllDevicesInfoAsync();

        Task<AqaraResponseArrayModel<ResourceValue>> GetDeviceResourceValueAsync(List<DeviceInfo> devicesInfo);
    }
}
