﻿using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using Microsoft.Extensions.Options;
using s1voded.home.config;
using s1voded.home.service.Aqara;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.service.InfluxDb
{
    public class InfluxDbService: IInfluxDbService
    {
        private readonly IInfluxDBClient _influxDbClient;
        private readonly IOptions<InfluxConfig> _influxConfig;

        public InfluxDbService(IInfluxDBClient influxDbClient, IOptions<InfluxConfig> influxConfig)
        {
            _influxDbClient = influxDbClient;
            _influxConfig = influxConfig;
        }

        public void WritePoint(PointData point)
        {
            using var writeApi = _influxDbClient.GetWriteApi();
            writeApi.WritePoint(point, _influxConfig.Value.Bucket, _influxConfig.Value.Org);
        }

    }
}
