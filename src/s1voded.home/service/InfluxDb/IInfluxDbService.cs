﻿using InfluxDB.Client.Writes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.service.InfluxDb
{
    public interface IInfluxDbService
    {
        void WritePoint(PointData point);
    }
}
