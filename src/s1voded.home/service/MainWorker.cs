using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using Microsoft.Extensions.Options;
using s1voded.home.config;
using s1voded.home.service.Aqara;
using s1voded.home.service.InfluxDb;
using s1voded.home.util;
using System.Xml.Linq;

namespace s1voded.home.service
{
    public class MainWorker : BackgroundService
    {
        private readonly ILogger<MainWorker> _logger;
        private readonly IAqaraApiService _aqaraApiService;
        private readonly IInfluxDbService _influxDbService;

        public MainWorker(ILogger<MainWorker> logger, IAqaraApiService aqaraApiService, IInfluxDbService influxDbService)
        {
            _logger = logger;
            _aqaraApiService = aqaraApiService;
            _influxDbService = influxDbService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                //get all device info
                var myDevicesInfo = await _aqaraApiService.GetAllDevicesInfoAsync();
                _logger.LogInformation("Total devices count: {count}", myDevicesInfo.Result.TotalCount);

                //get weather sensors from all sensors
                var myWeatherDevices = myDevicesInfo.Result.Data.Where(deviceInfo => deviceInfo.Model == Constant.WeatherDeviceModelName).ToList();
                _logger.LogInformation("Weather devices count: {count}", myWeatherDevices.Count);       

                while (!stoppingToken.IsCancellationRequested)
                {
                    //get all readings weather sensors
                    var myWeatherDevicesResourceValue = await _aqaraApiService.GetDeviceResourceValueAsync(myWeatherDevices);
                    _logger.LogInformation("Weather device resource value count: {count}", myWeatherDevicesResourceValue.Result.Count);

                    //create influxdb pointData for all measurements and write in db
                    foreach(var resourceValue in myWeatherDevicesResourceValue.Result)
                    {
                        string measurement = GetMeasurementType(resourceValue.ResourceId);

                        if (!string.IsNullOrEmpty(measurement))
                        {
                            var point = PointData
                                .Measurement(measurement)
                                .Tag("device", resourceValue.SubjectId)
                                .Field("value", Int32.Parse(resourceValue.Value))
                                .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

                            //write readings weather sensors
                            _influxDbService.WritePoint(point);
                        }
                    }

                    _logger.LogInformation("MainWorker get and write measurements at: {time}", DateTimeOffset.Now);
                    await Task.Delay(30000, stoppingToken);
                }

            }
            catch (Exception ex)
            {

            } 
        }

        private string GetMeasurementType(string resourceId)
        {
            string measurementType = "";

            switch (resourceId)
            {
                case Constant.WeatherDeviceTemperatureResourceId:
                    measurementType = "Temperature";
                    break;
                case Constant.WeatherDeviceHumidityResourceId:
                    measurementType = "Humidity";
                    break;
                case Constant.WeatherDevicePressureResourceId:
                    measurementType = "Pressure";
                    break;
                case Constant.DeviceBatteryResourceId:
                    measurementType = "Battery";
                    break;
                case Constant.DeviceZigbeeResourceId:
                    measurementType = "Zigbee";
                    break;
            }

            return measurementType;
        }
    }
}