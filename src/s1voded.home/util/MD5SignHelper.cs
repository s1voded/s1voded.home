﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.util
{
    public static class MD5SignHelper
    {
        //Sign Generation Rules: https://opendoc.aqara.com/en/docs/developmanual/apiIntroduction/signGenerationRules.html
        public static string CreateSign(string accessToken, string appId, string keyId, string nonce, string time, string appKey)
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(accessToken))
            {
                sb.Append("Accesstoken=").Append(accessToken).Append('&');
            }
            sb.Append("Appid=").Append(appId);
            sb.Append('&').Append("Keyid=").Append(keyId);
            sb.Append('&').Append("Nonce=").Append(nonce);
            sb.Append('&').Append("Time=").Append(time).Append(appKey);

            var signString = sb.ToString().ToLower();
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(signString));

            return Convert.ToHexString(hash);
        }

        public static string CreateSign(string appId, string keyId, string nonce, string time, string appKey)
        {
            return CreateSign("", appId, keyId, nonce, time, appKey);
        }
    }
}
