﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.util
{
    public static class Constant
    {
        //Request intent introduction: https://opendoc.aqara.com/en/docs/developmanual/apiIntroduction/requestIntent.html
        public const string RequestIntent_GetToken = "config.auth.getToken";
        public const string RequestIntent_QueryDeviceInfo = "query.device.info";
        public const string RequestIntent_QueryResourceValue = "query.resource.value";


        //Device's resource attribute details according to the weather device's model:
        //aqara temperature and humidity sensor model name
        public const string WeatherDeviceModelName = "lumi.weather.v1";

        //Temperature. The unit is 0.01℃. Reports when the change exceeds ±0.5℃, or reports with the humidity.
        //min: -4000; max: 12500
        public const string WeatherDeviceTemperatureResourceId = "0.1.85";

        //Humidity. The unit is one in ten thousand. Reports when the change exceeds ¡À6%, or reports with the temperature
        //min: 0; max: 10000
        public const string WeatherDeviceHumidityResourceId = "0.2.85";

        //Air pressure. The unit is Pa. Invalid value is 0. Reports with the temperature or humidity.
        //min: 30000; max: 110000
        public const string WeatherDevicePressureResourceId = "0.3.85";

        //Battery voltage value.The unit is mv.
        //min: 0; max: 3600
        public const string DeviceBatteryResourceId = "8.0.2008";

        //Zigbee signal strength
        //min: 0; max: 255
        public const string DeviceZigbeeResourceId = "8.0.2007";
    }
}
