﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.config
{
    public class InfluxConfig
    {
        public string Bucket { get; set; }

        public string Org { get; set; }

        public string AdminToken { get; set; }

        public int Port { get; set; }

        public string Host { get; set; }

        public string URL 
        {
            get
            {
                var builder = new UriBuilder("http", Host, Port);
                return builder.Uri.ToString();
            }
        }
    }
}
