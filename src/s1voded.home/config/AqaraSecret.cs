﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.config
{
    public class AqaraSecret
    {
        /// <summary>
        /// Aqara account (e.g. email, SMS)
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// AccessToken generated manually for 10 years (Expired Time:2032-08-30 09:47:33)
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// AppKey (manage project: https://developer.aqara.com/)
        /// </summary>
        public string AppKey { get; set; }
    }
}
