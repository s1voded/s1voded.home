﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s1voded.home.config
{
    public class AqaraConfig
    {
        /// <summary>
        /// Appid of the third-party project (manage project: https://developer.aqara.com/)
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// Keyid (manage project: https://developer.aqara.com/)
        /// </summary>
        public string KeyId { get; set; }

        /// <summary>
        /// Http address API
        /// </summary>
        public string URI { get; set; }
    }
}
